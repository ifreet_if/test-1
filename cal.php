<?php

class Cal {

    public $result = 0;

    public function add(float $var): float {
        
        $this->result += $var;
        
        return $this->result;
    }

    public function min(float $var): float {
        
        $this->result -= $var;
        
        return $this->result;
    }

    public function razdelit(float $digit): float {
        
        if($digit === 0) {
            throw new Exception('digit cannot be 0');
        }
        
        $this->result /= $digit;
        
        return $this->result;
    }

    public function mult(float $var): float {
        
        $this->result *= $var;
        
        return $this->result;
    }

    private function round(float $var): int {
        
        $this->result = round($var);
        
        return $this->result;
    }

}
